/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.pixscape.lux;

import java.awt.geom.Rectangle2D;
import java.awt.image.BandedSampleModel;
import org.locationtech.jts.geom.Point;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.Envelope2D;
import org.locationtech.jts.geom.Coordinate;
import org.thema.common.ProgressBar;
import org.thema.data.IOImage;
import org.thema.data.feature.Feature;
import org.thema.parallel.AbstractParallelTask;
import org.thema.pixscape.Bounds;
import org.thema.pixscape.Project;
import org.thema.pixscape.ScaleData;
import org.thema.pixscape.view.ViewShedResult;

/**
 *
 * @author gvuidel
 */
public class LuxTask extends AbstractParallelTask<Object, Object> {
    
    private static final double MIN_LUX = 0.01;
    
    private Project project;
    
    private List<Feature> points;
    
    /** The height of the observed points, -1 if not used */
    private double zDest;
    /** 
     * The default 3D limits of the sight. This limits are overriden by point layer attributes if present
     */
    private Bounds bounds;
    
    /** Result */
    private WritableRaster luxRast;

    public LuxTask(List<Feature> points, Project project, double zDest, Bounds bounds, 
            ProgressBar monitor) {
        super(monitor);
        this.points = points;
        this.project = project;
        this.zDest = zDest;
        this.bounds = bounds;
    }
    
    
    @Override
    public void init() {
        super.init();
        luxRast = Raster.createWritableRaster(new BandedSampleModel(DataBuffer.TYPE_FLOAT, project.getDtm().getWidth(), project.getDtm().getHeight(), 1), null);
    }


    @Override
    public Object getResult() {
        return luxRast;
    }

    @Override
    public Object execute(int start, int end) {
        //WritableRaster sumLux = Raster.createWritableRaster(new BandedSampleModel(DataBuffer.TYPE_FLOAT, project.getDtm().getWidth(), project.getDtm().getHeight(), 1), null);
        
        for(Feature point : points.subList(start, end)) {
            Point p = point.getGeometry().getCentroid();
            Bounds b = bounds.updateBounds(point);
            double zOrig = ((Number)point.getAttribute("height")).doubleValue();

            double flux = ((Number)point.getAttribute("lflux")).doubleValue(); // light flux in lumen
            double omega = 2*Math.PI*(Math.cos(Math.PI/180*(b.getZMin()+90)) - Math.cos(Math.PI/180*(b.getZMax()+90))); // solid angle in steradians
            double intens = flux / omega; // light intensity in candela
            
            b.setDmax(Math.sqrt(intens/MIN_LUX));
            
            ViewShedResult viewshed = 
                    project.getDefaultComputeView().calcViewShed(new DirectPosition2D(p.getX(), p.getY()), zOrig, zDest, false, b);
            
            ScaleData data = project.getDefaultScaleData();
            Coordinate c = data.getWorld2Grid().transform(p.getCoordinate(), new Coordinate());
            Rectangle2D r = new Rectangle2D.Double();
            r.setFrameFromCenter(c.x, c.y, c.x+b.getDmax()/data.getResolution(), c.y+b.getDmax()/data.getResolution());
            r = r.createIntersection(data.getGridGeometry().getGridRange2D());
            double cz = data.getDtmRaster().getSampleDouble((int)c.x, (int)c.y, 0) + zOrig;
            synchronized(this) {
                for(int y = (int) r.getMinY(); y < r.getMaxY(); y++) {
                    for(int x = (int) r.getMinX(); x < r.getMaxX(); x++) {
                        if(viewshed.getView().getSample(x, y, 0) > 0) {
                            double z = zDest == -1 ? data.getZ(x, y) : (data.getDtmRaster().getSampleDouble(x, y, 0) + zDest);
                            double d2 = Math.pow(viewshed.getRes2D()*(x-c.x), 2) + 
                                    Math.pow(viewshed.getRes2D()*(y-c.y), 2) + Math.pow(z-cz, 2);
                            //sumLux.setSample(x, y, 0, sumLux.getSampleDouble(x, y, 0) + intens / d2);
                        
                            luxRast.setSample(x, y, 0, luxRast.getSampleDouble(x, y, 0) + intens / d2);
                        }
                    }
                }
            }

            incProgress(1);
        }
        
        return null;//sumLux;
    }

    @Override
    public void gather(Object result) {
//        float[] luxTot = ((DataBufferFloat)luxRast.getDataBuffer()).getData();
//        float [] lux = ((DataBufferFloat)((Raster)result).getDataBuffer()).getData();
//        for(int i = 0; i < luxTot.length; i++) {
//             luxTot[i] += lux[i];
//        }
    }

    @Override
    public int getSplitRange() {
        return points.size();
    }
    
    public void saveResult(File dir, String name) throws IOException {
        if(dir == null) {
            dir = project.getDirectory();
        }
        
        Envelope2D env = project.getDtmCov().getEnvelope2D();
        env.setCoordinateReferenceSystem(project.getCRS());
        if(name == null) {
            name = "lux";
        }
        IOImage.saveTiffCoverage(new File(dir, name + ".tif"),
            new GridCoverageFactory().create("view", luxRast, env));
        
    }

}
